<!DOCTYPE html>
<html>

<head>
    <title>Ranking EscapeIFRS</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0; maximum-scale=1.0; user-scalable=0;">
    <link href="css/base.css" rel="stylesheet" type="text/css" />
    <link href="node_modules/jquery/toast-plugin/dist/jquery.toast.min.css" rel="stylesheet" type="text/css" />

    <!--    Tira as bolinhas padrões de uma lista do html     -->
    <style>
        #list {
            margin: 0px 25% 0px 25%;
            background-color: #333333;
            height: 60%;
            overflow-y: scroll;
        }
        #back {
            margin: 2.5% auto;
            text-align: center;
        }
        #link {
            text-decoration: none;
            color: white;
        }
        ul {
            margin: 0px;
            padding: 0px;
        }
        .table {
            text-align: center;
            height: 30px;
            color:white;
            width:100%;
            display:table;
            border-collapse: collapse;
        }
        .table-head {
            display:table-row;
            border-bottom: 1px solid #BBB;
        }
        .table-row {
            height: 60px;
            display:table-row;
            border-bottom: 1px solid #BBB;
        }
        .d{
            width:15%;
            display: table-cell;
            vertical-align: middle;
        }
    </style>

</head>

<body>

    <div style="font-size: 40px; color: white; text-align: center; vertical-align: middle; height:4%; width:100%; margin: 4% 0px"><b>RANKING</b></div>

    <div id="list">
        <div class="table" style="color:#BBB">
            <div class="table-head">
                <div class="d" style="width:15%;">N°</div>
                <div class="d" style="width:50%;">Nome</div>
                <div class="d" style="width:25%;">Matrícula</div>
            </div>
        </div>


        <div class="table" >
            <div class="table-row">
                <div class="d" style="width:15%;">1</div>
                <div class="d" style="width:50%;">Gabriel Viégas da Silva</div>
                <div class="d" style="width:25%;">02060124</div>
            </div>
        </div>





        <div class="table" >
            <div class="table-row">
                <div class="d" style="width:15%;">2</div>
                <div class="d" style="width:50%;">Augusto Falcão Flach</div>
                <div class="d" style="width:25%;">02060128</div>
            </div>
        </div>



        <div class="table" >
            <div class="table-row">
                <div class="d" style="width:15%;">3</div>
                <div class="d" style="width:50%;">Alex Júnior Padilha Pereira</div>
                <div class="d" style="width:25%;">02060126</div>
            </div>
        </div>


            <!--<div style="text-align: center; height: 30px; color:#white; width:100%; display:table; border-collapse: collapse;">
                <div style="display:table-row; border-bottom: 1px solid #BBB;">
                    <div style="width:15%; display: table-cell;  vertical-align: middle; border: 5px;">N°</div>
                    <div style="width:50%; display: table-cell;  vertical-align: middle;">Nome</div>
                    <div style="width:25%; display: table-cell;  vertical-align: middle;">Matrícula</div>
                </div>
            </div>-->



        </div>
        <div id="back"><a href="./index.html" id="link">Voltar</a></div>




        <script src="node_modules/jquery/dist/jquery.min.js" type="text/javascript"></script>
        <script>
            $.ajaxSetup({ cache: false });
        </script>


    </body>

    </html>
