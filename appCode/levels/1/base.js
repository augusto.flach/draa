 /**
  * Começaria na entrada do IF, com papeis e folhetos de jornal no chão situando
  *   um pouco a história e cada jornal no chão teria uma data e um código.
  *   (A data serviria para saber qual número vem primeiro no enigma), na porta
  *   de entrada teria algum tipo de enigma que seria usado os códigos dos jornais.
  *   Após o enigma, entrando no IF, após passar o portão, para abrir a porta principal
  *   poderia ser aquele joguinho de javascript que o senhor gostou
  *    (https://www.chiark.greenend.org.uk/~sgtatham/puzzles/js/net.html) adaptado.
  *    Vasculhar as salas e o nome de cada sala (em ordem) vai ter uma letra faltando
  *    (ex: DIRET RIA, a letra O, no caso seria a letra que está faltando) e no final,
  *    daria uma palavra. Cada sala vai ter um código (expressões lógicas) em ordem para
  *    a porta onde a palavra mandou. Caso ele n tenha feito a ordem certa da palavra,
  *    e foi direto para a porta (hall do if), ele não saberá a senha, e se não tiver
  *    feito as portas corretamente para dar a palavra, a senha estará na ordem errada.
  */

 /**
  * CRIAÇÃO DE NÍVEIS
  * MÉTODOS QUE PODEM SER USADOS!
  * <<Podem mudar, sugiro me avisar se tiver alguma ideia ou algum jeito que podem funcionar melhor>>
  *
  *


 exemplo de iframe


 callback = function(){
     if(iframe.passou === true) {
         iframe.passou = false;
         return true;
     }
     return false;
 }
 createIframe("/games/tri-logic/index.html#/course1/1", callback);





  *
  * Se criar uma área clicável, precisa adiciona-la na cena pra depois ser adicionada no game (pela engine) com o scene.addAreaClicavel.
  *
  *
  * O construtor da área clicavel recebe a posicao (x,y), o tamanho (width,height), uma função de callback para
  * ser executada após clicar na imagem e um texto para ser exibido na tela

  *
  *
  *
  *
  * IMPORTANTE!!
  * A função callback do ÁreaClicável acontece quando o cara clica no botão.
  *
  *
  * Para trocar de cena, crie um botão com a sprite 'seta' em 5o parametro e na function dele, char setScene(...);
  *
  *
  * DEFINIR PONTUAÇÃO NIVEL;
  *
  *
  * @type Level
  */






 //////////////////  INICIALIZANDO VARIAVEIS

 var level = new Level(1);
 $game.level = level;


 var cadeadoFlag = false;

 var desc1;
 var desc2;

 var playing = false;

 var luz = false;

 var n1 = 0,
     n2 = 0,
     n3 = 0,
     n4 = 0;

 var abriuNotinha1 = false;
 var abriuNotinha2 = false;

 var nc1 = 0,
     nc2 = 0;

 var $clicksMax = 200;
 var $challMax = 200;
 var $tempoChall = -100;
 var chaveCaixaLuz = false;
 var cadeadoRecepcao = false;
 var caixaLuzLigada = false;

 var respostaCaixaLuz = [];

 //////////////SORTEANDO NUMEROS CAIXA DE LUZ








 ////////////// INICIALIZANDO CENAS CRIADAS POSTERIORMENTE PARA O PHASER CARREGAR AS IMAGENS




 sceneE3 = new Scene('entrada.png', 'entradaClaroCompleta');
 level.addScene(sceneE3);
 sceneE3 = new Scene('recepcao.png', 'recepcaoClaro');
 level.addScene(sceneE3);
 sceneE3 = new Scene('corredorD1.png', 'corredorD1Claro');
 level.addScene(sceneE3);
 sceneE3 = new Scene('corredorE1.png', 'corredorE1Claro');
 level.addScene(sceneE3);
 sceneE3 = new Scene('painelD.png', 'painelDClaro');
 level.addScene(sceneE3);
 sceneE3 = new Scene('entradaSemi.png', 'entradaClaro');
 level.addScene(sceneE3);

 caixaLuz = new Scene('chaves.png', 'caixaLuzAberta');
 level.addScene(caixaLuz);
 caixaLuz = new Scene('chavesLigado.png', 'caixaLuzAbertaLigada');
 level.addScene(caixaLuz);








 ////////////      CENA INICIAL, "COMEÇAR"









 var sceneStart = new Scene('text.png', 'start', 'assets/general/');
 sceneStart.addAreaClicavel(new AreaClicavel(0, 200, $game.world.width, $game.world.height - 400, 'start', function() {

     if (lvl == 1) level.setScene('frente');
     else {
         for (var x = 1; x <= lvl; x++) {
             var now = false;
             if (x == lvl) now = true;
             typeof window['level' + (x)] === 'function' ? window['level' + (x)](now) : '';
         }

     }
     playing = true;
 }));
 level.addScene(sceneStart);

 sceneStart.start = function() {

 }









 //////////////// CENA DA FRENTE





 var frente = new Scene('frente.png', 'frente');
 frente.addAreaClicavel(new AreaClicavel(615, 450, 50, 50, 'seta', function() {
     level.setScene('portao');
 }, 90));

 frente.addAreaClicavel(new AreaClicavel(257, 544, 80, 50, 'page', function() {
     level.setScene('j1');
 }, 20));
 frente.addAreaClicavel(new AreaClicavel(350, 628, 80, 50, 'page', function() {
     level.setScene('j1');
 }, 200));
 frente.addAreaClicavel(new AreaClicavel(779, 607, 80, 50, 'page', function() {
     level.setScene('j1');
 }, 100));

 level.addScene(frente);


 ////////   START DA CENA DA FRENTE, TEXTOS




 frente.start = function() {
     var texto = [];
     texto.push(new Texto("Hoje faz dois meses..."));
     texto.push(new Texto("Dois meses desde que Sofia sumiu..."));
     texto.push(new Texto("E nunca mais apareceu."));
     texto.push(new Texto("Seus pais disseram que ela havia sido transferida para um campus do interior, mas\nela não foi desligada do sistema"));
     texto.push(new Texto("A última vez que a vi foi no prédio B"));
     texto.push(new Texto("Algo me diz que tem alguma coisa muito errada nessa história"));
     texto.push(new Texto("Consegui roubar as chaves do guardinha..."));
     texto.push(new Texto("E esta noite descobrirei a verdade!"));

     mostraTextos(texto);

     /*  texto.push(new Texto("e eles estão em \n\nordem", {nome: "Eu", recuo: 120}));
      texto.push(new Texto("esse ninguem falou"));
      /**/

     frente.start = {};

 }









 /////////////////////     JORNAL DO IF



 var j1 = new Scene('jornal1.png', 'j1', undefined, 500);
 j1.addAreaClicavel(new AreaClicavel($game.world.centerX, $game.world.height - 50, 50, 50, 'seta', function() {
     level.setScene('frente');
 }));
 level.addScene(j1);









 //////////////////   CENA DO PORTAO COM CADEADO E FOTINHO






 var portao = new Scene('portaoEscuro.png', 'portao');

 portao.addAreaClicavel(new AreaClicavel(0, $game.world.centerY, 50, 50, 'seta', function() {
     level.setScene('frente');
 }));

 portao.addAreaClicavel(new AreaClicavel(420, 445, 100, 110, '', function() {
     if (cadeadoFlag) {
         level.setScene("entrada");
     } else {
         level.setScene('cadeado');
     }
 }));
 portao.addAreaClicavel(new AreaClicavel($game.world.centerX - 75, 105, 100, 100, 'nota', function() {
     level.setScene('desafio');
 }));

 level.addScene(portao);





 /////////////// CENA DO DESAFIO, A NOTINHA DE CIMA DO PORTAO









 var desafio = new Scene('desafio.png', 'desafio');

 desafio.addAreaClicavel(new AreaClicavel($game.world.centerX, $game.world.height - 50, 50, 50, 'seta', function() {
     level.setScene('portao');
 }));

 desafio.start = function() {
     abriuNotinha1 = true;
 }
 level.addScene(desafio);









 ////////////////   CENA DO CADEADO



 var cadeado = new Scene('cadeado.jpg', 'cadeado');

 cadeado.addAreaClicavel(new AreaClicavel($game.world.centerX, $game.world.height - 50, 50, 50, 'seta', function() {
     level.setScene('portao');
 }));
 cadeado.start = function() {




     //////////// CRIANDO OS TEXTOS




     cadeado.t1 = $game.add.text(277, 517, n1, {
         font: "65px Garamond"
     });
     cadeado.t2 = $game.add.text(398, 510, n2, {
         font: "65px Garamond"
     });
     cadeado.t3 = $game.add.text(551, 500, n3, {
         font: "65px Garamond"
     });
     cadeado.t4 = $game.add.text(707, 497, n4, {
         font: "65px Garamond"
     });
     cadeado.t5 = $game.add.text(460, 420, n4, {
         font: "48px Garamond"
     });
     cadeado.t1.inputEnabled = true;
     cadeado.t2.inputEnabled = true;
     cadeado.t3.inputEnabled = true;
     cadeado.t4.inputEnabled = true;
     cadeado.t5.inputEnabled = true;
     cadeado.t5.text = 'ABRIR';



     ////////// ONCLICK DE CADA TEXTO



     cadeado.t1.events.onInputDown.add(function() {
         cadeado.t1.text = n1 = (Number(cadeado.t1.text) + 1) % 10;
         $pClicks += 10;

     }, this);
     cadeado.t2.events.onInputDown.add(function() {
         cadeado.t2.text = n2 = (Number(cadeado.t2.text) + 1) % 10;
         $pClicks += 10;
     }, this);
     cadeado.t3.events.onInputDown.add(function() {
         cadeado.t3.text = n3 = (Number(cadeado.t3.text) + 1) % 10;
         $pClicks += 10;

     }, this);
     cadeado.t4.events.onInputDown.add(function() {
         cadeado.t4.text = n4 = (Number(cadeado.t4.text) + 1) % 10;
         $pClicks += 10;

     }, this);
     cadeado.t5.events.onInputDown.add(function() {
         $pClicks += 10;
         checar();
     }, this);





     ///////////// CHECAGEM SE PASSOU DE FASE



     var emTexto = false;

     function checar() {
         if (cadeado.t1.text === '8' && cadeado.t2.text === '2' && cadeado.t3.text === '1' && cadeado.t4.text === '4') {

             if (abriuNotinha1) {
                 level2(true);
             } else {
                 if (!emTexto) {
                     var textos = [];
                     textos.push(new Texto("Trapacear é feio! Abra o desafio primeiro!!!"));
                     mostraTextos(textos, function() {
                         emTexto = false;
                     });
                     emTexto = true;
                 }
             }
         } else {

             if (!emTexto && !abriuNotinha1) {
                 var textos = [];
                 textos.push(new Texto("Dica: procure pelo desafio que abre o cadeado antes", {
                     'nome': 'Augusto F.',
                     'recuo': 50
                 }));
                 mostraTextos(textos, function() {
                     emTexto = false;
                 });
                 emTexto = true;
             }
             $game.camera.shake(0.01, 100);
             $pChall -= 50;
         }
     }
 }




 cadeado.end = function() {
     cadeado.t1.destroy();
     cadeado.t2.destroy();
     cadeado.t3.destroy();
     cadeado.t4.destroy();
     cadeado.t5.destroy();
 }
 level.addScene(cadeado);







 ////////////// PASSAGEM PARA O LEVEL 2 (FUNCAO)









 function level2(now = false) {

     if (playing) {
         $pChall += 100;
         limpaRegistros();
         finalizaLog();



         saveProgress(1, getPoints());
         lvl = 2;
         $("#lvl").html(lvl);


     }
     $pChall = 0;
     $pTime = 0;
     $pClicks = 0;

     $clicksMax = 200;
     $challMax = 300;
     $tempoMax = 150;
     cadeadoFlag = true;

     if (now) iniciaLog(function() {});
     level.setScene("entrada"); /**/
     var texto = [];
     if (now) {
         texto.push(new Texto("Parabéns, chegou ao nível 2\nAgora, encontre uma maneira de prosseguir"));

         mostraTextos(texto);
     }
     frente.start = function() {};

     getXp(function(res) {
         $('#xp').html(res);

     });
 }







 //////////////// CENA ENTRADA DO PREDIO









 sceneE = new Scene('entradaEscuro.png', 'entrada');
 sceneE.claro = false;


 //860 - 350
 // 960 640
 //90 - 350
 // 10 - 640



 sceneE.addAreaClicavel(new AreaClicavel($game.world.centerX, $game.world.height - 25, 40, 40, 'seta_r', function() {
     level.setScene('portao');
 }, 30));
 sceneE.addAreaClicavel(new AreaClicavel(860, 640, 40, 40, 'seta_r', function() {
     level.setScene('corredorD1');
 }));
 sceneE.addAreaClicavel(new AreaClicavel($game.world.width - 45, 350, 40, 40, 'seta_r', function() {
     level.setScene('painelD');
 }));


 sceneE.addAreaClicavel(new AreaClicavel(90, 640, 40, 40, 'seta', function() {
     level.setScene('corredorE1');
 }));
 sceneE.addAreaClicavel(new AreaClicavel(5, 350, 40, 40, 'seta', function() {
     level.setScene('recepcao');
 }));
 sceneE.addAreaClicavel(new AreaClicavel(470, 490, 40, 40, 'seta_f', function() {

     var textos = [];
     if (sceneR.claro) {

         textos.push(new Texto("Essas luzes estão queimadas há meses, melhor procurar alguma lanterna..."));
     } else {

         textos.push(new Texto("Muito escuro, melhor acender as luzes primeiro..."));
     }
     mostraTextos(textos);

 }), 'auditorios');








 /////////////// CORREDOR DA DIREITA ESCURO


 sceneCD1 = new Scene('corredorD1Escuro.png', 'corredorD1');
 sceneCD1.claro = false;



 sceneCD1.addAreaClicavel(new AreaClicavel($game.world.centerX, $game.world.height - 50, 50, 50, 'seta', function() {
     level.setScene('entrada');
 }));









 ////////////////// CORREDOR DA ESQUERDA ESCURO


 sceneCE1 = new Scene('corredorE1Escuro.png', 'corredorE1');
 sceneCE1.claro = false;


 sceneCE1.addAreaClicavel(new AreaClicavel($game.world.centerX, $game.world.height - 50, 50, 50, 'seta_r', function() {
     level.setScene('entrada');
 }, 15));


 // sceneCE1.addAreaClicavel(new AreaClicavel(180, 377, 30, 30, 'seta', function() {
 //   level.setScene('entrada');
 // }, 180, 'caixa_de_luz'));






 //////////// PAINEL DA DIREITA ESCURO




 scenePD = new Scene('painelDEscuro.png', 'painelD');
 scenePD.claro = false;



 scenePD.addAreaClicavel(new AreaClicavel(5, 350, 50, 50, 'seta', function() {
     level.setScene('entrada');
 }));



 //////////////////////// CENA DA NOTINHA DA ESCOLA


 var sceneNotaEscola = new Scene('notaRecepcao1.png', 'notaRecepcao', undefined, 500);

 sceneNotaEscola.addAreaClicavel(new AreaClicavel($game.world.centerX, $game.world.height - 25, 40, 40, 'seta_r', function() {

     level.setScene('recepcao');

 }, 30));

 sceneNotaEscola.addAreaClicavel(new AreaClicavel($game.world.width - 25, $game.world.centerY, 40, 40, 'seta_f', function() {

     level.setScene('notaRecepcao2');

 }, 90));

 level.addScene(sceneNotaEscola);





 var sceneNotaEscola2 = new Scene('notaRecepcao2.png', 'notaRecepcao2', undefined, 500);

 sceneNotaEscola2.addAreaClicavel(new AreaClicavel($game.world.centerX, $game.world.height - 25, 40, 40, 'seta_r', function() {

     level.setScene('recepcao');

 }, 30));




 sceneNotaEscola2.addAreaClicavel(new AreaClicavel(25, $game.world.centerY, 40, 40, 'seta_f', function() {

     level.setScene('notaRecepcao');

 }, 270));


 sceneNotaEscola2.addAreaClicavel(new AreaClicavel($game.world.width - 25, $game.world.centerY, 40, 40, 'seta_f', function() {

     level.setScene('enigmaRecepcao');

 }, 90));

 level.addScene(sceneNotaEscola2);








 var sceneEnigmaRecepcao = new Scene('enigmaRecepcao.png', 'enigmaRecepcao', undefined, 500);

 sceneEnigmaRecepcao.addAreaClicavel(new AreaClicavel($game.world.centerX, $game.world.height - 25, 40, 40, 'seta_r', function() {

     level.setScene('recepcao');

 }, 30));



 sceneEnigmaRecepcao.addAreaClicavel(new AreaClicavel(25, $game.world.centerY, 40, 40, 'seta_f', function() {

     level.setScene('notaRecepcao2');

 }, 270));


 level.addScene(sceneEnigmaRecepcao);









 //////////////// RECEPCAO ESQUERDA ESCURO




 sceneR = new Scene('recepcaoEscuro.png', 'recepcao');
 sceneR.claro = false;

 sceneR.addAreaClicavel(new AreaClicavel($game.world.width - 45, 350, 50, 50, 'seta_r', function() {
     level.setScene('entrada');
 }));


 //////////////  BOTAO NOTA DE EXPLICACAO



 sceneR.addAreaClicavel(new AreaClicavel(260, 390, 105, 35, '', function() {
     if (sceneR.claro) {

         abriuNotinha2 = true;
         level.setScene('notaRecepcao');



     } else {

         var texto = [];
         texto.push(new Texto("Assim não consigo ler, melhor acender as luzes..."));
         mostraTextos(texto);
     }
 }));




 /////   BOTAO DA LUZ, SE ESTÁ CLARO DESLIGA, SE ESTÁ ESCURO LIGA



 sceneR.addAreaClicavel(new AreaClicavel(41, 343, 37, 37, '', function() {
     if (sceneR.claro) esquerdaEscura();
     else {
         ligar();
     }
 }));






 ////////// FUNCAO DE LIGAR A LUZ





 ligar = function() {

     if (!luz) {

         callback = function() {

             if (iframe.passou === 1) {
                 return level3(true);
             }

             if (iframe.passou === 2) {
                 iframe.passou = false;

                 var texto = [];
                 texto.push(new Texto("O desafio não foi concluido com sucesso, tente novamente"));
                 mostraTextos(texto);
                 $pChall -= 50;
                 return true;
             }


             return false;
         }
         var tmp = Math.random();
         var num;
         do {
             num = (7 - (Math.floor(tmp * 3) + 1)) % 6 + 1;
         } while (num == desc1);
         desc1 = num;
         var texto = [];
         texto.push(new Texto("Para acender as luzes, um desafio de Descrições Narrativas é necessário!\nOrdene os passos para completar uma tarefa."));
         mostraTextos(texto, () => {
             createIframe("/games/tri-logic/index.html#/course1/" + num, callback);
         });

     } else {
         esquerdaClara();
     }

 }

 esquerdaClara = function(now = true, place = 'recepcao') {



     sceneEC = new Scene('entradaSemi.png', 'entrada');
     clonar(sceneEC, sceneE);
     sceneEC.claro = true;
     sceneE = sceneEC;
     level.removeScene('entrada');
     level.addScene(sceneE);


     sceneRC = new Scene('recepcao.png', 'recepcao');
     clonar(sceneRC, sceneR);
     sceneRC.claro = true;
     sceneR = sceneRC;
     level.removeScene('recepcao');
     level.addScene(sceneR);


     sceneCE1C = new Scene('corredorE1.png', 'corredorE1');
     clonar(sceneCE1C, sceneCE1);
     sceneCE1C.claro = true;
     sceneCE1 = sceneCE1C;
     level.removeScene('corredorE1');
     sceneCE1.addAreaClicavel(new AreaClicavel(180, 377, 30, 30, 'seta', function() {
         level.setScene('caixaLuz');
     }, 180, 'caixa_de_luz'));
     sceneCE1.addAreaClicavel(new AreaClicavel(90, 455, 35, 35, 'seta_r', function() {
         level.setScene('portaRecepcao');
     }, 180, 'portaRecepcao'));
     level.addScene(sceneCE1);



     scenePDC = new Scene('painelD.png', 'painelD');
     clonar(scenePDC, scenePD);
     scenePDC.claro = true;
     scenePD = scenePDC;
     level.removeScene('painelD');
     level.addScene(scenePD);


     if (caixaLuzLigada) {

         sceneCD1C = new Scene('corredorD1.png', 'corredorD1');
         clonar(sceneCD1C, sceneCD1);
         sceneCD1C.claro = true;
         sceneCD1 = sceneCD1C;
         sceneCD1.start = function() {
             if (!sceneR.claro) {
                 var texto = [];
                 texto.push(new Texto("Está muito escuro aqui, melhor voltar..."));
                 mostraTextos(texto, () => level.setScene('entrada'));

             } else {
                 textoFinalTeste(function() {
                     sceneCD1.start = function() {


                         if (!sceneR.claro) {
                             var texto = [];
                             texto.push(new Texto("Está muito escuro aqui, melhor voltar..."));
                             mostraTextos(texto, () => level.setScene('entrada'));

                         }

                     };
                 });

             }

         }
         level.removeScene('corredorD1');
         level.addScene(sceneCD1);

     }


     if (now) level.setScene("recepcao");
     else {
         level.setScene(place);
     }


 }


 esquerdaEscura = function(now = true, place) {


     sceneEC = new Scene('entradaEscuro.png', 'entrada');
     clonar(sceneEC, sceneE);
     sceneEC.claro = false;
     sceneE = sceneEC;
     level.removeScene('entrada');
     level.addScene(sceneE);


     sceneRC = new Scene('recepcaoEscuro.png', 'recepcao');
     clonar(sceneRC, sceneR);
     sceneRC.claro = false;
     sceneR = sceneRC;
     level.removeScene('recepcao');
     level.addScene(sceneR);


     sceneCE1C = new Scene('corredorE1Escuro.png', 'corredorE1');
     clonar(sceneCE1C, sceneCE1);
     sceneCE1C.claro = false;
     sceneCE1 = sceneCE1C;
     sceneCE1.removeAreaClicavel('caixa_de_luz');
     sceneCE1.removeAreaClicavel('portaRecepcao');
     level.removeScene('corredorE1');
     level.addScene(sceneCE1);



     scenePDC = new Scene('painelDEscuro.png', 'painelD');
     clonar(scenePDC, scenePD);
     scenePDC.claro = false;
     scenePD = scenePDC;
     level.removeScene('painelD');
     level.addScene(scenePD);


     if (chaveCaixaLuz) {

         sceneCD1C = new Scene('corredorD1Escuro.png', 'corredorD1');
         clonar(sceneCD1C, sceneCD1);
         sceneCD1C.claro = false;
         sceneCD1 = sceneCD1C;
         level.removeScene('corredorD1');
         level.addScene(sceneCD1);
     }


     if (now) level.setScene("recepcao");
     else {
         level.setScene(place);
     }




 }



 function level3(now = false) {




     if (playing) {
         $pChall += 100;
         limpaRegistros();
         finalizaLog();



         saveProgress(2, getPoints());
         lvl = 3;
         $("#lvl").html(lvl);


     }

     luz = true;
     triLogic = true;
     $pChall = 0;
     $pTime = 0;
     $pClicks = 0;

     $clicksMax = 200;
     $challMax = 300;
     $tempoMax = 150;

     if (now) iniciaLog();
     level.setScene("recepcao"); /**/
     var texto = [];
     if (now) {

         texto.push(new Texto("Parabéns, chegou ao nível 3\nAgora, encontre uma maneira de prosseguir"));
         mostraTextos(texto, () => {

             luz = true;
             esquerdaClara();
         });
     } else {
         luz = true;
         esquerdaClara();

     }



     iframe.passou = false;

     getXp(function(res) {
         $('#xp').html(res);

     });

     return true;




 }




 sceneCD1.start = function() {
     if (!sceneCD1.claro) {
         var texto = [];
         if (!sceneR.claro) {
             texto.push(new Texto("Está muito escuro aqui, melhor voltar..."));

         } else {
             texto.push(new Texto("Parece que este lado não acendeu, será que as chaves estão ligadas?"));

         }
         mostraTextos(texto, () => level.setScene('entrada'));
     }
 }
 sceneCE1.start = function() {
     if (!sceneCE1.claro) {
         var texto = [];
         texto.push(new Texto("Está muito escuro aqui, melhor voltar..."));
         mostraTextos(texto, () => level.setScene('entrada'));
     }
 }


 level.addScene(sceneE);
 level.addScene(sceneR);
 level.addScene(scenePD);
 level.addScene(sceneCD1);
 level.addScene(sceneCE1);









 var scenePortaRecepcao = new Scene("portaRecepcao.png", 'portaRecepcao');

 scenePortaRecepcao.addAreaClicavel(new AreaClicavel(685, 623, 160, 100, '', function() {
     if (cadeadoRecepcao) {
         level.setScene('recepcaoDentro');
     } else {
         level.setScene('cadeadoRecepcao');

     }
 }, 0, 'cadeadoRecepcao'));

 scenePortaRecepcao.addAreaClicavel(new AreaClicavel(25, $game.world.centerY, 50, 50, 'seta_r', function() {
     level.setScene('corredorE1');
 }, 180));

 level.addScene(scenePortaRecepcao);





 var sceneCadeadoRecepcao = new Scene("cadeadoRecepcao.png", 'cadeadoRecepcao');



 sceneCadeadoRecepcao.addAreaClicavel(new AreaClicavel($game.world.centerX, $game.world.height - 25, 40, 40, 'seta_r', function() {
     level.setScene('portaRecepcao');
 }, 30));

 level.addScene(sceneCadeadoRecepcao);



 sceneCadeadoRecepcao.start = function() {




     //////////// CRIANDO OS TEXTOS




     sceneCadeadoRecepcao.t1 = $game.add.text(584, 423, nc1, {
         font: "55px Garamond"
     });
     sceneCadeadoRecepcao.t2 = $game.add.text(687, 423, nc2, {
         font: "55px Garamond"
     });


     sceneCadeadoRecepcao.t1.inputEnabled = true;
     sceneCadeadoRecepcao.t2.inputEnabled = true;



     ////////// ONCLICK DE CADA TEXTO



     sceneCadeadoRecepcao.t1.events.onInputDown.add(function() {
         sceneCadeadoRecepcao.t1.text = nc1 = (Number(sceneCadeadoRecepcao.t1.text) + 1) % 10;
         $pClicks += 10;

     }, this);
     sceneCadeadoRecepcao.t2.events.onInputDown.add(function() {
         sceneCadeadoRecepcao.t2.text = nc2 = (Number(sceneCadeadoRecepcao.t2.text) + 1) % 10;
         $pClicks += 10;
     }, this);

 }


 sceneCadeadoRecepcao.addAreaClicavel(new AreaClicavel(0, 120, 840, 210, '', function() {






     var emTexto = false;








     if (sceneCadeadoRecepcao.t1.text === '2' && sceneCadeadoRecepcao.t2.text === '8') {


         if (abriuNotinha2) {
             level4(true);
         } else {
             if (!emTexto) {
                 var textos = [];
                 textos.push(new Texto("Trapacear é feio! Abra o desafio primeiro!!!"));
                 mostraTextos(textos, function() {
                     emTexto = false;
                 });
                 emTexto = true;
             }
         }



     } else {
         if (!emTexto && !abriuNotinha2) {
             var textos = [];
             if ($pChall < -50) {

                 textos.push(new Texto("Dica: o desafio está perto da entrada do prédio, do lado de dentro", {
                     'nome': 'Augusto F.',
                     'recuo': 50
                 }));
             } else {

                 textos.push(new Texto("Dica: procure pelo desafio que abre o cadeado antes", {
                     'nome': 'Augusto F.',
                     'recuo': 50
                 }));
             }
             mostraTextos(textos, function() {
                 emTexto = false;
             });
             emTexto = true;
         }
         $game.camera.shake(0.01, 100);
         $pChall -= 50;
     }
 }));




 sceneCadeadoRecepcao.end = function() {
     sceneCadeadoRecepcao.t1.destroy();
     sceneCadeadoRecepcao.t2.destroy();
 }




 function level4(now = false) {



     if (playing) {
         $pChall += 100;
         limpaRegistros();
         finalizaLog();



         saveProgress(3, getPoints());
         lvl = 4;
         $("#lvl").html(lvl);


     }

     $pChall = 0;
     $pTime = 0;
     $pClicks = 0;

     $clicksMax = 200;
     $challMax = 300;
     $tempoMax = 150;

     if (now) iniciaLog();
     level.setScene("cadeadoRecepcao"); /**/


     //scenePortaRecepcao.removeAreaClicavel('cadeadoRecepcao');

     cadeadoRecepcao = true;

     var textos = [];
     if (now) {

         textos.push(new Texto("Deu certo!\nAgora, a porta parece destrancada"));
         mostraTextos(textos, () => {
             luz = true;
             esquerdaClara(false, 'portaRecepcao');
             level.setScene('portaRecepcao');
         });
     }




     getXp(function(res) {
         $('#xp').html(res);

     });

     return true;




 }



 var sceneRecepcaoDentro = new Scene('recepcaoDentro.png', 'recepcaoDentro');

 sceneRecepcaoDentro.addAreaClicavel(new AreaClicavel($game.world.centerX, $game.world.height - 25, 40, 40, 'seta_f', function() {
     level.setScene('portaRecepcao');
 }, 180));

 sceneRecepcaoDentro.addAreaClicavel(new AreaClicavel(540, 450, 240, 120, '', function() {
     if (chaveCaixaLuz) {
         var textos = [];
         textos.push(new Texto("Já consegui a chave.\nMelhor ligar as luzes e continuar antes que alguém chegue!"));
         mostraTextos(textos);
     } else {
         fluxograma();
     }
 }));

 level.addScene(sceneRecepcaoDentro);



 function fluxograma() {

     if (!chaveCaixaLuz) {

         callback = function() {

             if (iframe.passou === 1) {
                 return level5(true);
             }

             if (iframe.passou === 2) {
                 iframe.passou = false;

                 var texto = [];
                 texto.push(new Texto("O desafio não foi concluido com sucesso, tente novamente"));
                 mostraTextos(texto);
                 $pChall -= 50;
                 return true;
             }


             return false;
         }
         var tmp = Math.random();

         var num;
         do {
             num = (7 - (Math.floor(tmp * 3) + 1)) % 6 + 1;
         } while (num == desc2 || num == desc1);
         desc2 = num;
         var texto = [];
         texto.push(new Texto("Para abrir a gaveta, outro desafio de Descrições Narrativas é necessário!\nOrdene os passos para completar uma tarefa."));
         mostraTextos(texto, () => {
             createIframe("/games/tri-logic/index.html#/course1/" + num, callback);
         });

     }









 }



 function level5(now = false) {




     if (playing) {
         $pChall += 100;
         limpaRegistros();
         finalizaLog();



         saveProgress(4, getPoints());
         lvl = 5;
         $("#lvl").html(lvl);


     }

     $pChall = 0;
     $pTime = 0;
     $pClicks = 0;

     $clicksMax = 200;
     $challMax = 300;
     $tempoMax = 150;

     if (now) iniciaLog();

     chaveCaixaLuz = true;

     level.setScene('recepcaoDentro');


     var textos = [];
     if (now) {

         textos.push(new Texto("Aleluia, consegui a chave!\nMelhor me apressar, não gostaria que me encontrassem aqui de noite"));
         mostraTextos(textos);

     }



     getXp(function(res) {
         $('#xp').html(res);

     });

     return true;









 }









 var caixaLuz = new Scene('caixaLuz.png', 'caixaLuz');
 caixaLuz.chave = false;

 caixaLuz.addAreaClicavel(new AreaClicavel($game.world.centerX, $game.world.height - 25, 40, 40, 'seta_f', function() {
     level.setScene('corredorE1');
 }, 180));

 caixaLuz.addAreaClicavel(new AreaClicavel(500, 570, 155, 120, '', function() {
     if (chaveCaixaLuz) {
         var textos = [];
         textos.push(new Texto('Caixa de Luz aberta com sucesso!'));
         mostraTextos(textos, function() {
             abrirCaixaLuz();
         });
     } else {
         var textos = [];
         textos.push(new Texto('Parece que preciso da chave para abrir isso...\nGeralmente as chaves ficam na recepção'));
         mostraTextos(textos);

     }
 }, 0, 'abrirCaixa'));



 function level6(now = false) {




     if (playing) {
         $pChall += 100;
         limpaRegistros();
         finalizaLog();



         saveProgress(5, getPoints());
         lvl = 6;
         $("#lvl").html(lvl);


     }

     $pChall = 0;
     $pTime = 0;
     $pClicks = 0;

     $clicksMax = 200;
     $challMax = 300;
     $tempoMax = 150;

     if (now) iniciaLog();


     caixaLuzLigada = true;

     esquerdaClara();
     graph.clear();
     montaCaixaLuz(now);
     var textos = [];
     if (now) {}



     getXp(function(res) {
         $('#xp').html(res);

     });

     return true;








 }




 function montaCaixaLuz(now = false) {

     level.removeScene('caixaLuz');
     caixaLuz = new Scene('chavesLigado.png', 'caixaLuz');
     level.addScene(caixaLuz);
     caixaLuz.addAreaClicavel(new AreaClicavel($game.world.centerX, $game.world.height - 25, 40, 40, 'seta_f', function() {
         level.setScene('corredorE1');
     }, 180));






     var vetBotoes = [
         [0, '0x00989e', '0x00dbe4'],
         [1, '0x000f9e', '0x0017f0'],
         [2, '0x9e0000', '0xe30000'],
         [3, '0x9e9500', '0xd7cb00'],
         [4, '0x009e13', '0x00cf19'],
         [5, '0x9e0091', '0xd000bf']
     ];

     var corOff = '0xb2b2b2';
     var corOn = '0xe7db00';

     var posic = [
         [301, 579],
         [390, 579],
         [481, 579],
         [571, 579],
         [661, 579],
         [751, 579]
     ];


     var posicCima = [
         [390, 365],
         [481, 365],
         [571, 365],
         [661, 365]
     ];






     caixaLuz.start = function() {
         graph.clear();
         text = $game.add.bitmapText(320, 135, 'carrier_command', 'SEQUENCIA ENCONTRADA', 15);
         downText = $game.add.bitmapText(330, 265, 'carrier_command', 'LUZES ACESAS!', 15);
         drawAllCirclesCimaOn();
         if (now) {

             now = false;
             var textos = [];
             textos.push(new Texto("Agora sim!\nPosso, enfim, seguir minha procura!"));
             mostraTextos(textos, function() {
                 drawAllCirclesOn();
                 caixaLuz.start = function() {

                     text = $game.add.bitmapText(320, 135, 'carrier_command', 'SEQUENCIA ENCONTRADA', 15);
                     downText = $game.add.bitmapText(330, 265, 'carrier_command', 'LUZES ACESAS!', 15);
                     drawAllCirclesCimaOn();
                     drawAllCirclesOn();
                 }

             });

         } else {

             text = $game.add.bitmapText(320, 135, 'carrier_command', 'SEQUENCIA ENCONTRADA', 15);
             downText = $game.add.bitmapText(330, 265, 'carrier_command', 'LUZES ACESAS!', 15);
             drawAllCirclesCimaOn();
             drawAllCirclesOn();
         }
     }



     caixaLuz.end = function() {

         graph.clear();

     }



     function drawAllCirclesCimaOn() {
         graph.beginFill(corOn, 1);
         for (var idx = 0; idx < 4; idx++) {
             graph.drawCircle(posicCima[idx][0], posicCima[idx][1], 33);
         }

     }


     function drawAllCirclesOn() {

         for (var idx = 0; idx < 6; idx++) {
             graph.beginFill(vetBotoes[idx][2], 1);
             graph.drawCircle(posic[idx][0], posic[idx][1], 33);
         }



     }

     level.setScene('caixaLuz');

 }




 var abrirCaixaLuz = function() {
     level.removeScene('caixaLuz');
     caixaLuz = clonar(new Scene('chaves.png', 'caixaLuz'), caixaLuz);
     caixaLuz.removeAreaClicavel('abrirCaixa');
     level.addScene(caixaLuz);


     var selected;

     var tentativasStart = 10;

     var vetTentativa = [];
     var tentativas;



     var text;
     var downText;

     //////      00989e     00dbe4     0
     //////      000f9e     0017f0     1
     //////      9e0000     e30000     2
     //////      9e9500     d7cb00     3
     //////      009e13     00cf19     4
     //////      9e0091     d000bf     5



     var vetBotoes = [
         [0, '0x00989e', '0x00dbe4'],
         [1, '0x000f9e', '0x0017f0'],
         [2, '0x9e0000', '0xe30000'],
         [3, '0x9e9500', '0xd7cb00'],
         [4, '0x009e13', '0x00cf19'],
         [5, '0x9e0091', '0xd000bf']
     ];

     var corOff = '0xb2b2b2';
     var corOn = '0xe7db00';

     var posic = [
         [301, 579],
         [390, 579],
         [481, 579],
         [571, 579],
         [661, 579],
         [751, 579]
     ];


     var posicCima = [
         [390, 365],
         [481, 365],
         [571, 365],
         [661, 365]
     ];



     function checaResposta() {
         var length = 0;
         for (var x = 0; x < 4; x++) {
             length += vetTentativa[x] !== undefined ? 1 : 0;
         }
         var corretas = 0;
         if (length === 4) {
             for (var x = 0; x < 4; x++) {
                 if (vetTentativa[x] === respostaCaixaLuz[x]) corretas++;
             }
             if (corretas == 4) {

                 level6(true);


             } else {
                 tentativas--;
                 setText(corretas + " CABOS EM POSICOES\n\nCORRETAS");
                 setDownText(tentativas + " TENTATIVAS RESTANTES");
                 if (tentativas < 1) {

                     tentativas = tentativasStart;
                     sorteiaRespostaCaixaLuz();
                     $game.camera.shake(0.01, 150);
                     drawAllCirclesCimaOff();
                     setText("TENTATIVAS ESGOTADAS!\n\nSISTEMA DE SEGURANCA\nFOI ATIVADO!\n\nCABOS SORTEADOS\nNOVAMENTE");
                     setDownText(tentativas + " TENTATIVAS RESTANTES");
                     $pChall -= 50;
                 } else {
                     setText(corretas + " CABOS EM POSICOES\n\nCORRETAS");
                     var s = '';
                     if (tentativas > 1) s = 'S';
                     setDownText(tentativas + " TENTATIVA" + s + " RESTANTES");
                     $game.camera.shake(0.005, 50);
                 }
             }

         } else {

         }


     }


     caixaLuz.addAreaClicavel(new AreaClicavel(posicCima[0][0] - 20, posicCima[0][1] - 20, 40, 40, '', function() {
         var num = 0;
         if (selected != undefined) {
             drawAllCirclesOff();
             drawCircleCimaOn(num, selected);
             vetTentativa['' + num] = selected;
             checaResposta();

             selected = undefined;
         } else {
             drawCircleCimaOff(num);
             vetTentativa[num] = undefined;
             checaResposta();
             selected = undefined;
         }
     }));





     caixaLuz.addAreaClicavel(new AreaClicavel(posicCima[1][0] - 20, posicCima[1][1] - 20, 40, 40, '', function() {
         var num = 1;
         if (selected != undefined) {
             drawAllCirclesOff();
             drawCircleCimaOn('' + num, selected);
             vetTentativa[num] = selected;
             checaResposta();

             selected = undefined;
         } else {
             drawCircleCimaOff(num);
             vetTentativa[num] = undefined;
             checaResposta();
             selected = undefined;
         }
     }));





     caixaLuz.addAreaClicavel(new AreaClicavel(posicCima[2][0] - 20, posicCima[2][1] - 20, 40, 40, '', function() {
         var num = 2;
         if (selected != undefined) {
             drawAllCirclesOff();
             drawCircleCimaOn('' + num, selected);
             vetTentativa[num] = selected;
             checaResposta();

             selected = undefined;
         } else {
             drawCircleCimaOff(num);
             vetTentativa[num] = undefined;
             checaResposta();
             selected = undefined;
         }
     }));





     caixaLuz.addAreaClicavel(new AreaClicavel(posicCima[3][0] - 20, posicCima[3][1] - 20, 40, 40, '', function() {
         var num = 3;
         if (selected != undefined) {
             drawAllCirclesOff();
             drawCircleCimaOn('' + num, selected);
             vetTentativa[num] = selected;
             checaResposta();

             selected = undefined;
         } else {
             drawCircleCimaOff(num);
             vetTentativa[num] = undefined;
             checaResposta();
             selected = undefined;
         }
     }));









     ///////////////////









     caixaLuz.addAreaClicavel(new AreaClicavel(posic[0][0] - 20, posic[0][1] - 20, 40, 40, '', function() {
         var num = 0;
         if (selected != num) {
             selected = num;
             drawAllCirclesOff();
             drawCircleOn(num);
         } else {
             selected = undefined;
             drawAllCirclesOff();
         }
     }));

     caixaLuz.addAreaClicavel(new AreaClicavel(posic[1][0] - 20, posic[1][1] - 20, 40, 40, '', function() {
         var num = 1;
         if (selected != num) {
             selected = num;
             drawAllCirclesOff();
             drawCircleOn(num);
         } else {
             selected = undefined;
             drawAllCirclesOff();
         }
     }));

     caixaLuz.addAreaClicavel(new AreaClicavel(posic[2][0] - 20, posic[2][1] - 20, 40, 40, '', function() {
         var num = 2;
         if (selected != num) {
             selected = num;
             drawAllCirclesOff();
             drawCircleOn(num);
         } else {
             selected = undefined;
             drawAllCirclesOff();
         }
     }));

     caixaLuz.addAreaClicavel(new AreaClicavel(posic[3][0] - 20, posic[3][1] - 20, 40, 40, '', function() {
         var num = 3;
         if (selected != num) {
             selected = num;
             drawAllCirclesOff();
             drawCircleOn(num);
         } else {
             selected = undefined;
             drawAllCirclesOff();
         }
     }));

     caixaLuz.addAreaClicavel(new AreaClicavel(posic[4][0] - 20, posic[4][1] - 20, 40, 40, '', function() {
         var num = 4;
         if (selected != num) {
             selected = num;
             drawAllCirclesOff();
             drawCircleOn(num);
         } else {
             selected = undefined;
             drawAllCirclesOff();
         }
     }));

     caixaLuz.addAreaClicavel(new AreaClicavel(posic[5][0] - 20, posic[5][1] - 20, 40, 40, '', function() {
         var num = 5;
         if (selected != num) {
             selected = num;
             drawAllCirclesOff();
             drawCircleOn(num);
         } else {
             selected = undefined;
             drawAllCirclesOff();
         }
     }));

     caixaLuz.start = function() {

         drawAllCirclesOff();
         drawAllCirclesCimaOff();
         sorteiaRespostaCaixaLuz();

         tentativas = tentativasStart;


         text = $game.add.bitmapText(320, 135, 'carrier_command', '  LIGUE OS CABOS PARA\n\n    ATIVAR A ENERGIA', 15);
         downText = $game.add.bitmapText(330, 265, 'carrier_command', tentativas + ' TENTATIVAS RESTANTES', 15);



     }

     function setText(texto) {
         text.text = texto;
     }

     function setDownText(texto) {
         downText.text = texto;
     }



     caixaLuz.end = function() {

         graph.clear();

     }



     function drawCircleCimaOn(idx, color) {
         graph.beginFill(vetBotoes[color][2], 1);
         graph.drawCircle(posicCima[idx][0], posicCima[idx][1], 33);
     }

     function drawCircleCimaOff(idx) {
         graph.beginFill(corOff, 1);
         graph.drawCircle(posicCima[idx][0], posicCima[idx][1], 33);
     }


     function drawAllCirclesCimaOff() {
         vetTentativa.splice(0, 4);
         graph.beginFill(corOff, 1);
         for (var idx = 0; idx < 4; idx++) {
             graph.drawCircle(posicCima[idx][0], posicCima[idx][1], 33);
         }

     }

     function drawAllCirclesCimaOn() {
         graph.beginFill(corOn, 1);
         for (var idx = 0; idx < 4; idx++) {
             graph.drawCircle(posicCima[idx][0], posicCima[idx][1], 33);
         }

     }



     function drawCircleOn(idx) {
         graph.beginFill(vetBotoes[idx][2], 1);
         graph.drawCircle(posic[idx][0], posic[idx][1], 33);
     }

     function drawCircleOff(idx) {
         graph.beginFill(vetBotoes[idx][1], 1);
         graph.drawCircle(posic[idx][0], posic[idx][1], 33);
     }


     function drawAllCirclesOn() {

         for (var idx = 0; idx < 6; idx++) {
             graph.beginFill(vetBotoes[idx][2], 1);
             graph.drawCircle(posic[idx][0], posic[idx][1], 33);
         }



     }

     function drawAllCirclesOff() {

         for (var idx = 0; idx < 6; idx++) {
             graph.beginFill(vetBotoes[idx][1], 1);
             graph.drawCircle(posic[idx][0], posic[idx][1], 33);
         }


     }





     level.setScene('caixaLuz');
 }



 level.addScene(caixaLuz);





 function sorteiaRespostaCaixaLuz() {

     var vetTemp = [0, 1, 2, 3, 4, 5];
     respostaCaixaLuz = [];

     for (var x = 0; x < 4; x++) {
         respostaCaixaLuz.push(vetTemp[Math.round(Math.random() * 5)]);

     }


 };









 level.setStart("start");





 function textoFinalTeste(callback) {

     $('#gameInventory').html('<br><br><br><br><button onclick="window.open(\'https://goo.gl/forms/NyNPD8Fo9rMwLbMt2\',\'__blank\')">Responda o questionario do trabalho aqui, por favor</button>');
     var textos = [];
     textos.push(new Texto('Versão Demo concluida com sucesso! Parabéns!!', {
         'nome': 'Augusto F.',
         'recuo': 50
     }));
     textos.push(new Texto('Obrigado por jogar o DRAA.\nAs fases do jogo seguem em desenvolvimento, atualmente.', {
         'nome': 'Augusto F.',
         'recuo': 50
     }));
     textos.push(new Texto('Se tiver qualquer sugestão, ou tiver achado algum bug, peço que me avise.', {
         'nome': 'Augusto F.',
         'recuo': 50
     }));
     textos.push(new Texto('Por enquanto são poucos puzzles.\nMas quem tiver chegado até aqui, retire seu brinde comigo.', {
         'nome': 'Augusto F.',
         'recuo': 50
     }));
     textos.push(new Texto('Gostaria que respondessem o questionário na área da direita da tela\nMuito obrigado pela participação!', {
         'nome': 'Augusto F.',
         'recuo': 50
     }));
     mostraTextos(textos, callback);

 }








 clonar = function(scene1, scene2) {
     scene1.setAreasClicaveis(scene2.getAreasClicaveis());
     scene1.start = scene2.start;
     scene1.end = scene2.end;
     return scene1;

 }
