//Constante global para carregamento de telas
/**
 * Constantes para não precisar digitar o caminho inteiro do código
 * ex de código para o nível 3: (_LEVEL_BASE + '3') >>>> ('./appCode/levels/3')
 * @type String
 */
var _TPL_BASE = './appCode/view/';
var _LEVEL_BASE = './appCode/levels/';

/**
 * Nível atual sendo jogado
 * $ no começo da variável representa que a variável é global (pode ser chamada de qualquer arquivo)
 * @type Number
 */
var $level = 1;


var $pClicks = 0;
var $pChall = 0;
var $pTime = 0;
var $pBase = 0;
var tempoSeg = 0;

var registros = [];
var lockRegistros = false;
var saiuTexto;

var largTela;


var logId;

var xpAtual = 0;


var lvl = 1;



var graph;


var triLogic = false;

/**
 * Variável que contém o objeto do jogo (que herda e tem os métodos do Phaser)
 * $ no começo da variável representa que a variável é global (pode ser chamada de qualquer arquivo)
 * @type Game
 */
var $game = null;


var iframe = {};


var checkUpdate;


/**
 * Definindo o que acontecerá quando a página for carregada
 * Deixa a tela redimensionável e carrega as pgenerales da tela
 * @type type
 */
$(document).ready(function() {
    window.addEventListener('resize', resize, false);
    resize();
    montaTela();
});

/**
 * Método para redimensionar a tela (É o que ficou como Event Listener do window, é chamado sempre que a tela é redimensionada)
 * @returns {undefined}
 */
function resize() {
    var largMaxima = $(window).width() * 0.7;
    largTela = largMaxima;
    $("#gameContainer").width(largMaxima);
    $("#iframe").width(largMaxima);

}

/**
 * Arruma o menu, aba de jogo e inventário na página
 * @returns {undefined}
 */
function montaTela() {
    setLevel(function(lvlRes) {


        iniciaLog(function() {
            montaFase();
            montaMenu(function() {

                $('#lvl').html(lvlRes);
                getXp(function(res) {
                    $('#xp').html(res);

                });
            });

        });







    });


}

/**
 * Arruma a pgenerale mais da esquerda da tela (Menu com definições do usuário jogando)
 * @returns {undefined}
 */
function montaMenu(callback) {

    //TODO fazer vir do servidor os dados que precisa para o menu
    getDadosUser(function(data) {
        processaTpl('gameMenu', "#gameStats", {
            'user': JSON.parse(data),
        }, callback);
    });
}

/**
 * Monta a segunda pgenerale da tela, a central, que contém o jogo
 * @returns {undefined}
 */
function montaFase() {

    $game = new Game('gameContainer', {
        preload: preload,
        create: create,
        update: update
    });

    //Atualmente herda de Phaser.Game

    function preload() {
        getPreload($level);
        $game.load.image('back', 'assets/general/back.png');
        $game.load.image('seta', 'assets/general/seta.png');
        $game.load.image('seta_f', 'assets/general/seta_f.png');
        $game.load.image('seta_r', 'assets/general/seta_r.png');
        $game.load.image('start', 'assets/general/start.png');
        $game.load.audio('back_sound', 'assets/sounds/back_sound.mp3');
        $game.load.image('bgBtn', 'assets/general/backgroundButtons.png');
        $game.load.image('texto', 'assets/general/text.png');
        $game.stage.disableVisibilityChange = true;
        $game.load.bitmapFont('carrier_command', 'assets/fonts/carrier_command.png', 'assets/fonts/carrier_command.xml');

    }

    function update() {
        if ($pClicks >= 0) {
            $pClicks = 0;
        }
        if ($pChall >= 0) {
            $pChall = 0;
        }

        if (registros.length >= 10 && !lockRegistros) {
            limpaRegistros();
        }


        if (typeof checkUpdate === 'function') {
            checkUpdate();
        }


        $game.world.bringToTop(graph);


    }


    function create() {

        $("#iframe").hide();
        $game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
        $game.scale.setMinMax(350, 250, 1000, 900);
        $("#gameContainer").removeClass('loading');
        $game.stage.backgroundColor = '#2d2d2d';
        $game.background = $game.add.sprite($game.world.centerX, $game.world.centerY, 'back');
        $game.background.anchor.set(0.5); //coloca a imagem no centro.
        $game.addAmbientSound('back_sound'); //adiciona uma música ambiente
        var level = $game.loadLevel($level);

        $game.canvas.addEventListener('mousedown', function() {
            //console.log("X - " + $game.input.x + " | Y - " + $game.input.y);
        });


        graph = $game.add.graphics(0, 0);




        $game.canvas.addEventListener('mousedown', function() {
            if (!$game.level.current.texto) {

                $pClicks -= 10;
                if (logId !== undefined && !$game.level.current.texto) {
                    if (saiuTexto) {
                        saiuTexto = false;
                    } else {

                        var obj = {
                            'id_log': logId,
                            'registro_horario': pgFormatDate(new Date()),
                            'acerto': 'false',
                            'registro_info': 'Informação não especificada'
                        };
                        registros.push(obj);
                        lockRegistros = true;
                    }

                }
                setTimeout(function() {
                    //console.log("clicks | " + $pClicks + "\ntempo | " + $pTime + "\ndesafio | " + $pChall);
                    lockRegistros = false;
                    //console.log(registros);
                }, 200);
            }
        });

        setInterval(function() {

            $pTime -= 1;
            //console.log("time | " + $pTime);

        }, 1000);


    }

}

function getPoints() {
    var tempo = 0;
    if ($pTime < $tempoChall) tempo = $pTime - $tempoChall;


    if ($pClicks >= 0) {
        $pClicks = 0;
    }
    if ($pChall >= 0) {
        $pChall = 0;
    }

    var total = $pClicks + tempo + $pChall + $challMax + $clicksMax;
    if (total <= 100) total = 100;
    xpAtual += total;
    return total;
}

/**
 * Monta a terceira e última pgenerale da tela (da direita) que contém os itens que o usuário possui dentro de um inventário
 * @returns {undefined}
 */
function montaInventario() {

}
