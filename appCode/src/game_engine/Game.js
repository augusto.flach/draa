/**
 * Construtor do Jogo
 * Fica dentro da variavel global $game - pode ser acessado de qualquer lugar - e fica dentro da div central da página
 * Recebe o id da div em que vai ficar e as funções a serem chamadas para configura-lo
 * @param {string} div - div onde vai ser colocado o game
 * @param {object} funcs description
 * @returns Game
 */
var Game = function(div, funcs) {
    var width = $("#" + div).width();
    var height = $("#" + div).height();
    this.clicables = [];

    //Desabilita botao direito
    $("#" + div).on("contextmenu", function() {
        return false;
    });

    Phaser.Game.call(this,
        1000, 700, //Largura e altura do jogo
        Phaser.AUTO, //Sistema de render openGL ou canvas
        div, funcs);
    new Phaser.ScaleManager(this, width, height); // Função de escala que  redimenciona para  o tamanho padrão

    /**
     * Adiciona uma nova area clicavel no game
     * @param {type} area
     * @returns {undefined}
     */
    this.addAreaClicavel = function(area) {
        var button = this.add.button(area.x, area.y, area.bgImg, function() {
            area.click();
        }, this); //Adicionando "área clicavel"
        area.btn = button;
        button.input.useHandCursor = false; //removendo mão
        if (typeof area.ang !== 'undefined') {
            $game.add.tween(button).to({
                angle: 180
            }, Phaser.Easing.Linear.None, true); //aparentemente é assim que gira......
            button.angle = area.ang;
            button.anchor.setTo(0.5, 0.5);
        }
        button.width = area.w;
        button.height = area.h;
        button.visible = area.show;
        this.clicables.push(button);
    };

    /**
     * Inicia o nivel em parametro (nome da pasta)
     * @param {type} lvlTmp
     * @returns {undefined}
     */
    this.loadLevel = function(lvlTmp) {
        requireLevelJS(lvlTmp, function(data) {
            // Executa o JS do arquivo
            //eval(data);
            //Quando dá o eval é como se tivesse executado o código aqui dentro
            //Então tudo o que foi criado ou feito lá está dentro desse método.
            //O "level" é um exemplo. A variável é declarada lá dentro do base.js do nivel
            $game.level = level;
            $game.loader({
                //Carrega cada uma das imagens do nivel
                load: function() {
                    var imagens = level.getManifest().imagens;
                    for (var i in imagens) {
                        $game.load.image(i, imagens[i].path + imagens[i].backLoader);
                    };
                    $game.load.start();
                },
                //Quando completar executa:
                complete: function() {
                    $game.processaLevel(level)
                }
            })
        });
    };

    /**
     *
     * Recebe o Level criado no arquivo js do nível carregado
     *
     * @param {type} lvlTmp
     * @returns {undefined}
     */
    this.processaLevel = function(lvlTmp) {
        //Destrói aquele background criado enquanto o jogo é carregado
        this.background.destroy();
        //Adiciona uma imagem no lugar do background anterior. É a imagem da primeira cena do nivel
        this.background = this.add.sprite(this.world.centerX, this.world.centerY, lvlTmp.start.back);
        this.background.width = $game.camera.width;
        this.background.height = $game.camera.height;
        //Centraliza a imagem
        this.background.anchor.set(0.5);

        game = this;
        //        criaNavegacao();
        //Processa a cena inicial do nivel
        this.processaScene(lvlTmp.start);


        lvlTmp.start.start();

    };

    /**
     * Arruma a troca de cenas
     * Remove os clicaveis atuais da tela e bota os novos da cena do parametro
     *
     * @param {type} scene
     * @returns {undefined}
     */
    this.processaScene = function(scene) {
        this.removeClicaveis();
        this.preparaAreasClicaveis(scene.getAreasClicaveis());

    };

    this.removeClicaveis = function() {
        for (var a in $game.clicables) {
            $game.clicables[a].destroy();
        }
        $game.clicables = [];
    }

    this.preparaAreasClicaveis = function(areas) {
        for (var a in areas) {
            $game.addAreaClicavel(areas[a]);
        }
    }

    this.disableButtons = function() {
        for (var a in $game.clicables) {
            $game.clicables[a].inputEnabled = false;
        }
    }

    this.enableButtons = function() {
        for (var a in $game.clicables) {
            $game.clicables[a].inputEnabled = true;
        }
    }

    /**
     * É pra ver o quanto ta carregando o jogo, aparece branco em cima da página enquanto carrega a cena
     * @param {type} objeto
     * @returns {undefined}
     */
    this.loader = function(objeto) {
        //this.load.onLoadStart.add(loadStart, this);
        this.load.onFileComplete.add(this.loaderFileComplete, this);
        this.load.onLoadComplete.add(this.loaderComplete, this);
        this.textLoad = this.add.text(32, 32, 'Loading...', {
            fill: '#ffffff'
        });

        this.loaderCorrente = objeto;
        objeto.load();
    };

    /**
     * Funcao apontada no OnFileCOmplete no loader
     * @param {type} progress
     * @param {type} cacheKey
     * @param {type} success
     * @param {type} totalLoaded
     * @param {type} totalFiles
     * @returns {undefined}
     */
    this.loaderFileComplete = function(progress, cacheKey, success, totalLoaded, totalFiles) {
        this.textLoad.setText("File Complete: " + progress + "% - " + totalLoaded + " out of " + totalFiles);
    };
    /**
     * Funcao linkada no OnLoadComplete do Loader
     * @returns {undefined}
     */
    this.loaderComplete = function() {
        this.textLoad.destroy();
        this.loaderCorrente.complete();
    };

    /**
     * Define os sons do mapa
     * É criada no game, mas vai pegar a especifica de cada nível. Se o nivel não tiver uma, segue com a ambiente
     * @param {type} backSound
     * @returns {undefined}
     */
    this.addAmbientSound = function(backSound) {
        //Seta o ambienceSound (Phaser) como o parametro recebido (caminho do arquivo)
        this.ambianceSound = this.add.audio(backSound);
        //Passa o som, a funcao callback (que da play nele) e o objeto onde será tocado (jogo)
        this.sound.setDecodedCallback([backSound], this.ambientSound, this);
    }

    /**
     * Dá play na música do jogo (ambienceSound)
     * @returns {undefined}
     */
    this.ambientSound = function() {
        this.ambianceSound.play("", -1000, 1, true);
    };

};
/**
 * Faz Game herdar do Phaser
 */
herda(Phaser.Game, Game);
