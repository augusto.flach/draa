/**
 * Construtor de uma cena.
 * Ainda a ser mudado, mas ele possui a próxima cena e a cena anterior
 * Tem uma imagem de background (parametro)
 * Cena tem start e stop, pra quando entrar e sair dela
 * Cenan também tem pause e continue, pra parar os botões
 * Também tem um array de áreas que podem ser clicadas para (criadas com o addAreaClicavel)
 * @param {type} back
 * @returns {Scene}
 */
var Scene = function(back, id, path) {
    this.id = id;
    this.back = back;
    this.backLoader = {
        'backLoader': back,
        'path': '',
    };

    this.texto = false;

    var path = arguments[2];
    if (typeof path === 'undefined') {
        path = 'assets/' + level.id + '/';
    }


    this.backX = arguments[3];
    this.backY = arguments[4];



    this.backLoader.path = path;

    var areasClicaveis = [];

    /**
     * Adiciona mais uma área clicável ao array
     * @param {type} areaClicavel
     * @returns {undefined}
     */
    this.addAreaClicavel = function(areaClicavel) {
        areasClicaveis.push(areaClicavel);

    };






    this.removeAreaClicavel = function(nome) {
        for (var key in areasClicaveis) {
            if (areasClicaveis[key].nome === nome) {
                areasClicaveis.splice(key, 1);
            }
        }
    }


    /**
     * Retorna um array com todas as áreas clicáveis da cena
     * @returns {Array|Scene.areasClicaveis}
     */
    this.getAreasClicaveis = function() {
        return areasClicaveis;
    };

    this.seeImage = function() {

    }


    this.setAreasClicaveis = function(novas) {
        areasClicaveis = novas;
    };


    this.pause = function() {
        $game.disableButtons();
    };

    this.continue = function() {
        $game.enableButtons();
    };

};
