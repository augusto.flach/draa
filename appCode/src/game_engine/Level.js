/**
 * Construtor do level
 * Possui uma lista de senhas, onde inciar, onde está no momento, itens, etc
 * @param {type} id
 * @returns {Level}
 */
 function Level(id) {
    //Privadas
    var scenes = [];

    //Publicas
    this.start = {};
    this.current = this.start;
    this.passwords = {};
    this.itens = {};
    this.id = id;

    /**
     * Array com os assets do nivel
     * Tem os sons e as imagens do mesmo
     * @type type
     */
     var manifest = {
        sounds: {},
        imagens: {}
    };

    this.getScene = function(id) {
        for (var x in scenes) {
            if (id === scenes[x].id) {
                return scenes[x];
            }
        }
        return '0';
    };

    this.setScene = function(id) {
        var scene = this.getScene(id);
        if (scene !== '0') {
            if (typeof this.current.end === "function") {
                this.current.end();
            }
            this.current = scene;
            $game.background.destroy();
            $game.background = $game.add.sprite($game.world.centerX, $game.world.centerY, scene.back);
            $game.background.width = typeof scene.backX === 'undefined' ? $game.camera.width : scene.backX;
            $game.background.height = typeof scene.backY === 'undefined' ? $game.camera.height : scene.backY;
            $game.background.anchor.set(0.5);
            $game.stage.backgroundColor = '#000000';
            $game.processaScene(scene);
            $('#auxiliar').prop("checked", false);
            if (typeof this.current.start === "function") {
                this.current.start();
            }

        }
    }



    /**
     * 
     * @returns {Level.manifest}
     */
     this.getManifest = function() {
        /**
         * Passa por todas as cenas do nivel pegando suas imagens de fundo
         * @returns {unresolved}
         */
         function getImagensCena() {
            var map = {};

            for (var s in scenes) {
                map[scenes[s].backLoader.backLoader] = scenes[s].backLoader;
            }

            return map;
        }

        /**
         * Código pra adicionar o retorno do getImagensCena ao imagens do manifest
         */
         manifest.imagens = Object.assign(manifest.imagens, getImagensCena());

         return manifest;
     };

    /**
     * Retorna um array com todas as cenas do nivel
     * @returns {Array|Level.scenes}
     */
     this.getScenes = function() {
        return scenes;
    };

    /**
     * Adiciona uma cena no nivel e define, se um jeito meio tosco, qual a cena anterior e a posterior a ela NELA
     * @param {type} scene
     * @returns {undefined}
     */
     this.addScene = function(scene) {
        if (scenes.length === 0) {
            this.start = this.current = scene;
            if (typeof this.current.start === "function") {
                this.current.start();
            }
        }
        scenes.push(scene);
    };

    this.startScene = function() {
        if (typeof this.current.start === "function") {
            this.start.start();
        }
    }

    this.setStart = function(scene) {
        this.start = this.current = this.getScene(scene);
        if (typeof this.current.start === "function") {
            this.current.start();
        }

    }


    this.removeScene = function(id) {
        for (var x in scenes) {
            if (id === scenes[x].id) {
                scenes.splice(x, 1);
            }
        }
        return '0';
    };


};
