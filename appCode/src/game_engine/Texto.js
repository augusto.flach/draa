/**

Só dar um new texto, passar o texto (\n funciona)
Pode ou não passar um callback e um personagem


personagem precisa ter nome e/ou recuo (pra encaixar melhor na tela)






o show mostra na tela

Se o cara clicar, o texto vai de vez
Ele desabilita botões até o cara sair do texto

Dá pra chamar outro texto pelo callback










/**/




var Texto = function(texto, personagem, callback) {
  var obj = this;
  this.texto = texto;
  this.callback = callback;
  var img;
  var text, textoVet, interval, autor, recuo, nome;
  var i = 0;

  this.show = function() {
    $game.level.current.pause();
    $game.level.current.texto = true;

    img = $game.add.sprite(0, $game.camera.height - 250, "texto");

    img.scale.x = $game.camera.width / img.width;
    img.scale.y = 275 / img.height;

    text = $game.add.text(60, $game.camera.height - 190, '', {
      font: "18px",
      fill: "#FFFFFF"
    });
    if (typeof personagem !== "undefined") {
      recuo = typeof personagem.recuo === "undefined" ? 0 : personagem.recuo;
      nome = typeof personagem.nome === "undefined" ? '' : personagem.nome;
    }
    autor = $game.add.text($game.camera.width - 250 + recuo, $game.camera.height - 215, nome, {
      font: "23px",
      fill: "#FFFFFF"
    });
    textoVet = texto.split("");
    var tempo = 40;
    if (texto.length >= 30) tempo = 30;
    if (texto.length >= 40) tempo = 20;
    interval = setInterval(function() {
      if (i >= texto.length) {
        clearInterval(interval);
      } else {
        text.text = text.text.concat(textoVet[i]);
        i++;
      }
    }, tempo);

    setTimeout(function() {
      $game.input.onDown.addOnce(end);
    }, 300);

  };
  this.end = function() {
    if (i < texto.length) {
      clearInterval(interval);
      text.text = texto;
      i = texto.length;
      setTimeout(function() {
        $game.input.onDown.addOnce(end);
      }, 200);
    } else {
      img.destroy();
      autor.destroy();
      text.destroy();
      $game.level.current.continue();
      $game.level.current.texto = false;
      saiuTexto = true;

      if (typeof obj.callback === "function") obj.callback();
      $pClicks += 10;
    }
  }
  var end = this.end;

}
