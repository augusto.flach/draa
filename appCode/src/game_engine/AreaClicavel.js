/**
 * Construtor de uma area clicavel.
 * Recebe a posicao x,y, o tamanho, w,h e a funcão que ele executa
 * @param {type} x
 * @param {type} y
 * @param {type} w
 * @param {type} h
 * @param {type} callback
 * @returns {AreaClicavel}
 */
var AreaClicavel = function(x, y, w, h, bgImg, callback, ang, nome) {
    this.x = x;
    this.y = y;
    this.w = w;
    this.h = h;
    this.bgImg = bgImg;
    this.ang = ang;
    this.callback = callback;
    this.nome = nome;
    this.btn = {};
    this.show = true;
    this.point = 40;


    this.hide = function(hide) {
        this.show = hide;
        this.btn.visible = hide;
    }
    this.click = function() {

        var obj = {
            'id_log': logId,
            'registro_horario': pgFormatDate(new Date()),
            'acerto': 'true',
            'registro_info': 'Informação não especificada'
        };
        if (this.desc !== undefined) {
            obj.registro_info = this.desc;
        }
        lockRegistros = false;
        registros.push(obj);

        $pClicks += this.point;
        this.point = 10;

        this.callback();
    }
}
