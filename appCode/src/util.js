/**
 * Inútil
 * @type Array
 */
var CSSReq = new Array();

/**
 * TPL = Template (arquivo html externo)
 * Ele carrega um arquivo HTML externo, preenchendo campos com {{...}}
 * MÉTODO IMPORTANTE
 * Passar uma string com O NOME do arquivo a ser carregado (o caminho até lá ele completa)
 * Se passar uma função como 4 argumento, ele vai executar ela após carregar o TPL
 * @param {type} tpl
 * @param {type} div
 * @param {type} dados
 * @returns {undefined}
 */
function processaTpl(tpl, div, dados) {
    var funcaoCallback = arguments[3];

    var tpl = _TPL_BASE + '/' + tpl + '.html';

    // Para quem não sabe, no get tu manda que arquivo tu quer ler e recebe o conteudo dele de volta. o function é o que executa depois de pegar o arquivo
    $.get(tpl, function(conteudoArquivo) {
            //Passa cada objeto dentro do array Dados para o Processa(), preenchendo os campos do html
            // D é o nome do objeto ("Char" : 1234) >> Char é o nome, 1234 é o conteúdo (dados[d]
            for (var nomeObj in dados) {
                conteudoArquivo = processa(dados[nomeObj], nomeObj, conteudoArquivo);
            }
            $(div).html(conteudoArquivo);
            $(div).removeClass('loading');
            if (typeof funcaoCallback === 'function') {
                funcaoCallback();
            }
        }

    );
    /**
     * Preenche os campos com {{...}} no html
     * Se obj for um objeto com mais objetos dentro, ele vai ir de um em um preenchendo os dados
     * Ex no game: Passa um objeto com um outro objeto user dentro. No for no método lá em cima, ele passa
     * cada um dos objetos pra essa função. Aí, ele entra em cada uma das string que tem dentro desse outro
     * objeto (user)
     * @param {type} obj
     * @param {type} nomeObjeto
     * @returns {undefined}
     */
    function processa(obj, nomeObjeto, conteudoArquivo) {
        for (var nomeAtributo in obj) {
            if ((typeof obj[nomeAtributo]) === "object") {
                conteudoArquivo = processa(obj[nomeAtributo], nomeAtributo, conteudoArquivo);
            }
            var pt = nomeObjeto + "." + nomeAtributo;
            var rg = new RegExp("{{" + pt + "}}", 'g');
            conteudoArquivo = conteudoArquivo.replace(rg, obj[nomeAtributo]);
        }
        return conteudoArquivo;
    }

}

function saveProgress(lvl, xp) {

    $.post('/gameService/saveProgress', {
        'id_missao': lvl,
        'xp': xp
    }, function(data) {});

}






function pgFormatDate(date) {


    function zeroPad(d) {
        return ("0" + d).slice(-2)
    }


    var parsed = new Date(date)

    var str = '' + parsed.getUTCFullYear() + '-' + zeroPad(parsed.getMonth() + 1) + '-' + zeroPad(parsed.getDate()) + ' ' + zeroPad(parsed.getHours()) + ':' + zeroPad(parsed.getMinutes()) + ':';
    str += zeroPad(parsed.getSeconds()) + '.000000';
    return str;
}









function processaSemTpl(tpl, div) {
    var funcaoCallback = arguments[2];

    var tpl = _TPL_BASE + '/' + tpl + '.html';
    // Para quem não sabe, no get tu manda que arquivo tu quer ler e recebe o conteudo dele de volta. o function é o que executa depois de pegar o arquivo
    $.get(tpl,
        function(conteudoArquivo) {
            //Passa cada objeto dentro do array Dados para o Processa(), preenchendo os campos do html
            // D é o nome do objeto ("Char" : 1234) >> Char é o nome, 1234 é o conteúdo (dados[d]
            $(div).html(conteudoArquivo);
            $(div).removeClass('loading');
            if (typeof funcaoCallback === 'function') {
                funcaoCallback();
            }
        }

    );
}


var acmInicia = 0;

function iniciaLog(callback) {
    var obj =
        $.post('/gameService/iniciaLog', {
            'id_missao': lvl,
            'hora_inicio': pgFormatDate(new Date())
        }, function(data) {
            if (!(data > 0)) {
                if (acmInicia >= 5) {
                    acmInicia = 0;
                    idLog = undefined;
                    typeof callback === 'function' ? callback(0) : '';
                    window.location = '/login/logout';
                    return;

                }
                iniciaLog(callback);
                acmInicia++;
            } else {
                logId = data;
                typeof callback === 'function' ? callback(1) : '';
            }
        });
}






function limpaRegistros() {

    while (registros.length > 0) {
        registraLog(registros.shift());
    }


}





function registraLog(obj) {
    if (obj.id_log === undefined) return;

    $.post('/gameService/registro', obj, function(data) {});
}

var acm = 0;

function finalizaLog(callback) {
    if (logId === undefined) return;
    var obj = {
        'id_log': logId,
        'hora_fim': pgFormatDate(new Date())
    };
    $.post('/gameService/finalizaLog', obj, function(data) {
        if (data !== 1) {
            if (acm >= 5) {
                acm = 0;
                logId = undefined;
                typeof callback === 'function' ? callback(0) : '';
                return;
            }
            finalizaLog(callback);
            acm++;
        } else {
            logId = undefined;
            typeof callback === 'function' ? callback(1) : '';
        }
    });
}









/**
 * Carrega o JS de um nivel
 * @param {type} lvl
 * @param {type} callback
 * @returns {undefined}
 */
function requireLevelJS(lvl, callback) {
    var levelFile = _LEVEL_BASE + '/' + lvl + '/base.js';
    $.get(levelFile,
        function(data) {
            typeof callback === 'function' ? callback(data) : '';
        }
    );
}

/**
 * Não faz absolutamente nada, mas vai carregar um CSS externo
 * @param {type} css
 * @returns {undefined}
 */
function requireCSS(css) {
    if (null !== CSSReq[css]) {

    }
}

/**
 * Ainda não usado, mas diz se uma variavel está vazia
 * @param {type} variavel
 * @returns {Boolean}
 */
function isNull(variavel) {
    if (typeof(variavel) !== 'undefined' && variavel != null) {
        return false;
    } else {
        return true;
    }
}

// Sobrescrevendo o object.create para fazer a herança no 'herda'
if (typeof Object.create !== 'function') {
    Object.create = function(o) {
        function F() {}
        F.prototype = o;
        return new F();
    };
}

/**
 * Faz com que, quando um objeto chame o herda, ele herde do prototype de um outro objeto mãe
 * O 'Game', que possui todo o jogo chama esse método para herdar e ter todos os métodos do Phaser
 * @param {type} mae
 * @param {type} filha
 * @returns {undefined}
 */
function herda(mae, filha) {
    // Faz uma cópia do prototipo da mãe
    var copiaDaMae = Object.create(mae.prototype);
    // herda mãe
    filha.prototype = copiaDaMae;
    //Ajusta construtor da filha
    filha.prototype.constructor = filha;
}

function showButtons() {
    if ($('#auxiliar').is(':checked')) {
        for (var x in $game.clicables) {
            var button = $game.clicables[x];
            button.blur = $game.add.sprite(button.x, button.y, 'bgBtn');
            button.blur.width = button.width;
            button.blur.height = button.height;
        }

    } else {
        for (var x in $game.clicables) {
            var button = $game.clicables[x];
            if (button.blur !== '') {
                button.blur.kill();
            }
        }
    }
}

function getDadosUser(callback) {
    var user = {};
    $.get('/gameService/requireUserEscape', function(data) {
        callback(data);
    });


}






/**
 * posteriormente vai checar qual o nível e setar
 * @returns {undefined}
 */
function setLevel(callback) {
    $.get('/gameService/getLevel', function(res) {
        if (res.replace(/ /g, '') === '') lvl = 1;
        else lvl = parseInt(res) + 1;
        typeof callback === 'function' ? callback(lvl) : '';
    });
}

function getXp(callback) {

    $.get('/gameService/getXp', function(data) {
        if (data.replace(/ /g, '') === '') xpAtual = 0;
        else xpAtual = parseInt(data);
        typeof callback === 'function' ? callback(xpAtual) : '';
    });

}









function getPreload(level) {
    $.get(_LEVEL_BASE + '/' + level + '/preload.js', function(data) {

        $game.loader({
            'load': function() {
                eval(data);
                $game.load.start();
            },
            'complete': function() {
                return (data);
            }
        });
    });
}


function mostraTextos(vet, callback) {

    for (var i = 0; i < vet.length - 1; i++) {

        vet[i].num = i + 1;
        vet[i].callback = function() {
            vet[this.num].show();
        };

    }
    vet[vet.length - 1].callback = callback;
    vet[0].show();


}

function createIframe(path, callback) {

    $("#iframe").html('<iframe id="iframeWindow" src="' + path + '" style="border:0px; width:100%; height: 100%;">');
    $("#gameContainer").hide();
    $("#iframe").show();
    triLogic = true;
    document.getElementById("iframeWindow").contentWindow.console.log = function() {}
    checkUpdate = function() {
        if (callback()) {
            $("#iframe").hide();
            $("#gameContainer").show();
            checkUpdate = "";
            triLogic = false;
        }
    }

}
