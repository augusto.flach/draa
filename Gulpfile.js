/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


// Aqui nós carregamos o gulp e os plugins através da função `require` do nodejs
var gulp = require('gulp');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');
var rename = require('gulp-rename');
var sourcemaps = require('gulp-sourcemaps');

// Definimos o diretorio dos arquivos para evitar repetição futuramente
var files = "./appCode/src/**/*.js";
// Diretórios dos levels
var levels = "./appCode/levels/";

//Criamos outra tarefa com o nome 'dist'
gulp.task('dist', function () {

// Carregamos os arquivos novamente
// E rodamos uma tarefa para concatenação
// Renomeamos o arquivo que sera minificado e logo depois o minificamos com o `uglify`
// E pra terminar usamos o `gulp.dest` para colocar os arquivos concatenados e minificados na pasta build/
    gulp.src(files)
       .pipe(sourcemaps.init())
            .pipe(concat('./js'))
            .pipe(rename('dist.min.js'))
            .pipe(uglify())
        .pipe(sourcemaps.write()) 
        .pipe(gulp.dest('./js/build'));
        //.pipe(plugins.livereload());
});

//Criamos uma tarefa 'default' que vai rodar quando rodamos `gulp` no projeto
gulp.task('default', function () {
// Usamos o `gulp.run` para rodar as tarefas
// E usamos o `gulp.watch` para o Gulp esperar mudanças nos arquivos para rodar novamente
    gulp.run('dist');
    gulp.watch(files, function (evt) {
        gulp.run('dist');
    });
});